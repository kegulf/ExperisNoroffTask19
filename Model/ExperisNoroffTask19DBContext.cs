﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperisNoroffTask19.Model {
    class ExperisNoroffTask19DBContext : DbContext {

        public DbSet<Supervisor> Supervisors { get; set; }

        public DbSet<Student> Students { get; set; }
    }
}
