﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperisNoroffTask19.Model {
    public abstract class Person {

        private int id;
        private string name;


        public Person() {}

        public Person (int id, string name) {
            this.id = id;
            this.name = name;
        }

        public override string ToString() {
            return Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
