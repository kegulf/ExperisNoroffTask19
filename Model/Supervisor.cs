﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperisNoroffTask19.Model {
    public class Supervisor : Person {

        public Supervisor() { }
        public Supervisor(int id, string name) : base(id, name) { }

        public override string ToString() {
            return $"Prof. {base.ToString()}";
        }
    }
}
