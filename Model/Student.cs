﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperisNoroffTask19.Model {
    public class Student : Person {

        public Student() {
        }

        public Student(int id, string name, Supervisor supervisor) : base(id, name) {
            Supervisor = supervisor;
            SupervisorId = supervisor.Id;
        }

        public Supervisor Supervisor { get; set; }
        public int SupervisorId { get; set; }
    }
}
