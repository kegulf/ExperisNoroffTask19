﻿using ExperisNoroffTask19.Model;
using ExperisNoroffTask19.View;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExperisNoroffTask19 {
    public partial class MainForm : Form {

        private ExperisNoroffTask19DBContext dbContext;


        public MainForm() {
            InitializeComponent();

            dbContext = new ExperisNoroffTask19DBContext();


            /*
                I FOUND THE F-ing BUG!
                So it turns out that the DbSets from EF don't get populated until
                you make a reference to them.
                
                I don't access the Supervisor list until I open my Create/Edit popup window. 
                Therefore the Supervisors connected to the Students in the Student DbSet is 
                not loaded when I try to show the students in GridView. That is also why 
                all the supervisor names appears as soon as I create a new Student. 
                
                That was a hard bug to figure out xD
                
                The following line is a workaround that forces the DbSet to load.
             */

            dbContext.Supervisors.ToList();

            UpdateGridView();

        }

        private void CreatePersonButton_Click(object sender, EventArgs e) {

            Student newStudent = new Student();

            CreateAndEditForm createAndEditForm = new CreateAndEditForm(true);

            createAndEditForm.SetStudentToEdit(newStudent);
            createAndEditForm.AddSupervisorsToDropdownList(
                dbContext.Supervisors.ToArray());

            createAndEditForm.ShowDialog();

            if(createAndEditForm.IsOkPressed()) {
                dbContext.Students.Add(newStudent);
                dbContext.SaveChanges();
                UpdateGridView();
            }

            createAndEditForm.Dispose();
        }

        private void UpdateGridView() {
            BindingSource bindingSource = new BindingSource();

            bindingSource.DataSource =
                from s in dbContext.Students.ToList()
                select new {
                    s.Id, s.Name, SupervisorName = s.Supervisor.Name
                };

            personListGridView.DataSource = bindingSource;    
            personListGridView.Refresh();
        }


        private void EditPersonButton_Click(object sender, EventArgs e) {

            DataGridViewSelectedRowCollection selectedRows = personListGridView.SelectedRows;

            if (selectedRows.Count == 1 || personListGridView.CurrentCell != null) {

                int rowIndex = personListGridView.CurrentCell.RowIndex;
                int id = (int)personListGridView.Rows[rowIndex].Cells[0].Value;

                Student studentToEdit = dbContext.Students.Find(id);

                CreateAndEditForm caeForm = new CreateAndEditForm(false);

                caeForm.AddSupervisorsToDropdownList(dbContext.Supervisors.ToArray());
                caeForm.SetStudentToEdit(studentToEdit);
                caeForm.ShowDialog();

                if (caeForm.IsOkPressed()) {
                    dbContext.SaveChanges();
                    UpdateGridView();
                }

                caeForm.Dispose();
            }

            else {
                AlertForm alertForm = new AlertForm("Please select a student to edit.");
                alertForm.ShowDialog();
                alertForm.Dispose();
                return;
            }
        }

        private void DeletePersonButton_Click(object sender, EventArgs e) {

            DataGridViewSelectedRowCollection selectedRows = personListGridView.SelectedRows;

            if (selectedRows.Count == 1 || personListGridView.CurrentCell != null) {

                int rowIndex = personListGridView.CurrentCell.RowIndex;
                int id = (int) personListGridView.Rows[rowIndex].Cells[0].Value;
                Student studentToDelete = dbContext.Students.Find(id);

                string messageToUser = $"Are you sure you want to delete {studentToDelete}?";

                ConfirmChoiceForm ccForm = new ConfirmChoiceForm(messageToUser);

                ccForm.ShowDialog();

                if ( ccForm.IsOkClicked() ) {
                    dbContext.Students.Remove(studentToDelete);
                    dbContext.SaveChanges();
                    UpdateGridView();
                }
                ccForm.Dispose();
            }

            else {
                AlertForm alertForm = new AlertForm("No student selected.");
                alertForm.ShowDialog();
                alertForm.Dispose();
                return;
            }
        }

        private void ConvertToJSONButton_Click(object sender, EventArgs e) {

            string jsonString = JsonConvert.SerializeObject(dbContext.Supervisors, Formatting.Indented);

            jsonOutputTextBox.Text = jsonString;

        }
    }
}
