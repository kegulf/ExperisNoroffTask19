﻿namespace ExperisNoroffTask19 {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.deletePersonButton = new System.Windows.Forms.Button();
            this.createPersonButton = new System.Windows.Forms.Button();
            this.editPersonButton = new System.Windows.Forms.Button();
            this.personListGridView = new System.Windows.Forms.DataGridView();
            this.uiHeaderLabel = new System.Windows.Forms.Label();
            this.convertToJSONButton = new System.Windows.Forms.Button();
            this.jsonOutputTextBox = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.personListGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // deletePersonButton
            // 
            this.deletePersonButton.Location = new System.Drawing.Point(497, 513);
            this.deletePersonButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.deletePersonButton.Name = "deletePersonButton";
            this.deletePersonButton.Size = new System.Drawing.Size(203, 46);
            this.deletePersonButton.TabIndex = 1;
            this.deletePersonButton.Text = "Delete Person";
            this.deletePersonButton.UseVisualStyleBackColor = true;
            this.deletePersonButton.Click += new System.EventHandler(this.DeletePersonButton_Click);
            // 
            // createPersonButton
            // 
            this.createPersonButton.Location = new System.Drawing.Point(43, 513);
            this.createPersonButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.createPersonButton.Name = "createPersonButton";
            this.createPersonButton.Size = new System.Drawing.Size(203, 46);
            this.createPersonButton.TabIndex = 3;
            this.createPersonButton.Text = "Create Person";
            this.createPersonButton.UseVisualStyleBackColor = true;
            this.createPersonButton.Click += new System.EventHandler(this.CreatePersonButton_Click);
            // 
            // editPersonButton
            // 
            this.editPersonButton.Location = new System.Drawing.Point(271, 513);
            this.editPersonButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.editPersonButton.Name = "editPersonButton";
            this.editPersonButton.Size = new System.Drawing.Size(203, 46);
            this.editPersonButton.TabIndex = 4;
            this.editPersonButton.Text = "Edit Person";
            this.editPersonButton.UseVisualStyleBackColor = true;
            this.editPersonButton.Click += new System.EventHandler(this.EditPersonButton_Click);
            // 
            // personListGridView
            // 
            this.personListGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.personListGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.personListGridView.Location = new System.Drawing.Point(43, 92);
            this.personListGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.personListGridView.MultiSelect = false;
            this.personListGridView.Name = "personListGridView";
            this.personListGridView.RowHeadersWidth = 62;
            this.personListGridView.RowTemplate.Height = 28;
            this.personListGridView.Size = new System.Drawing.Size(657, 402);
            this.personListGridView.TabIndex = 6;
            // 
            // uiHeaderLabel
            // 
            this.uiHeaderLabel.AutoSize = true;
            this.uiHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiHeaderLabel.Location = new System.Drawing.Point(37, 31);
            this.uiHeaderLabel.Name = "uiHeaderLabel";
            this.uiHeaderLabel.Size = new System.Drawing.Size(367, 31);
            this.uiHeaderLabel.TabIndex = 7;
            this.uiHeaderLabel.Text = "Faculty and student overview";
            // 
            // convertToJSONButton
            // 
            this.convertToJSONButton.Location = new System.Drawing.Point(741, 513);
            this.convertToJSONButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.convertToJSONButton.Name = "convertToJSONButton";
            this.convertToJSONButton.Size = new System.Drawing.Size(415, 46);
            this.convertToJSONButton.TabIndex = 8;
            this.convertToJSONButton.Text = "Convert Supervisors to JSON";
            this.convertToJSONButton.UseVisualStyleBackColor = true;
            this.convertToJSONButton.Click += new System.EventHandler(this.ConvertToJSONButton_Click);
            // 
            // jsonOutputTextBox
            // 
            this.jsonOutputTextBox.Location = new System.Drawing.Point(741, 92);
            this.jsonOutputTextBox.Name = "jsonOutputTextBox";
            this.jsonOutputTextBox.ReadOnly = true;
            this.jsonOutputTextBox.Size = new System.Drawing.Size(415, 402);
            this.jsonOutputTextBox.TabIndex = 9;
            this.jsonOutputTextBox.Text = "";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 585);
            this.Controls.Add(this.jsonOutputTextBox);
            this.Controls.Add(this.convertToJSONButton);
            this.Controls.Add(this.deletePersonButton);
            this.Controls.Add(this.createPersonButton);
            this.Controls.Add(this.editPersonButton);
            this.Controls.Add(this.uiHeaderLabel);
            this.Controls.Add(this.personListGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Entity Framework Demo";
            ((System.ComponentModel.ISupportInitialize)(this.personListGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button deletePersonButton;
        private System.Windows.Forms.Button createPersonButton;
        private System.Windows.Forms.Button editPersonButton;
        private System.Windows.Forms.DataGridView personListGridView;
        private System.Windows.Forms.Label uiHeaderLabel;
        private System.Windows.Forms.Button convertToJSONButton;
        private System.Windows.Forms.RichTextBox jsonOutputTextBox;
    }
}

