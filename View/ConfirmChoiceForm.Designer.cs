﻿namespace ExperisNoroffTask19.View {
    partial class ConfirmChoiceForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.alertMessageLabel = new System.Windows.Forms.Label();
            this.headlineLabel = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.confirmButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // alertMessageLabel
            // 
            this.alertMessageLabel.AutoSize = true;
            this.alertMessageLabel.Location = new System.Drawing.Point(17, 54);
            this.alertMessageLabel.MaximumSize = new System.Drawing.Size(290, 0);
            this.alertMessageLabel.MinimumSize = new System.Drawing.Size(290, 0);
            this.alertMessageLabel.Name = "alertMessageLabel";
            this.alertMessageLabel.Size = new System.Drawing.Size(290, 34);
            this.alertMessageLabel.TabIndex = 6;
            this.alertMessageLabel.Text = "asdfkasd klasdf kasdf mlasdf mklasdf mklasdf mklasd fmklasdf ømkl";
            // 
            // headlineLabel
            // 
            this.headlineLabel.AutoSize = true;
            this.headlineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headlineLabel.Location = new System.Drawing.Point(12, 9);
            this.headlineLabel.Name = "headlineLabel";
            this.headlineLabel.Size = new System.Drawing.Size(243, 29);
            this.headlineLabel.TabIndex = 4;
            this.headlineLabel.Text = "Please confirm action";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(169, 138);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(138, 39);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // confirmButton
            // 
            this.confirmButton.Location = new System.Drawing.Point(17, 138);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(138, 39);
            this.confirmButton.TabIndex = 8;
            this.confirmButton.Text = "OK";
            this.confirmButton.UseVisualStyleBackColor = true;
            this.confirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // ConfirmChoiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 196);
            this.Controls.Add(this.confirmButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.alertMessageLabel);
            this.Controls.Add(this.headlineLabel);
            this.Name = "ConfirmChoiceForm";
            this.Text = "ConfirmChoiceForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label alertMessageLabel;
        private System.Windows.Forms.Label headlineLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button confirmButton;
    }
}