﻿using ExperisNoroffTask19.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExperisNoroffTask19.View {
    public partial class CreateAndEditForm : Form {

        private Student studentToEdit;
        private bool isOkPressed;


        public CreateAndEditForm(bool isNewStudent) {
            InitializeComponent();

            isOkPressed = false;

            headlineLabel.Text = (isNewStudent) ? "Add new student" : "Edit student info";
        }


        #region EventHandlers

        private void ConfirmButton_Click(object sender, EventArgs e) {

            string name = this.nameTextBox.Text;
            Supervisor supervisor = (Supervisor)this.supervisorComboBox.SelectedItem;

            string errorMessage;

            if( !InputsAreValid(name, supervisor, out errorMessage) ) {
                new AlertForm(errorMessage).ShowDialog();
            }

            else {
                studentToEdit.Name = name;
                studentToEdit.Supervisor = supervisor;
                studentToEdit.SupervisorId = supervisor.Id;

                isOkPressed = true;

                this.Close();
            }
        }

        internal bool IsOkPressed() {
            return isOkPressed;
        }

        private void CancelButton_Click(object sender, EventArgs e) {
            this.Close();
        }

        #endregion


        #region Behaviour

        /// <summary>
        ///     Checks that the name is atleast two characters
        ///     and that there has been selected a supervisor.
        /// </summary>
        /// <param name="name">
        ///     The inputted name
        /// </param>
        /// <param name="supervisor">
        ///     The chosen supervisor
        /// </param>
        /// <param name="errorMessage">
        ///     A string marked as out, this is usable if the method returns false.
        /// </param>
        /// <returns>
        ///     true if name is atleast two chars long and supervisor != null
        ///     false if oposite.
        /// </returns>
        private bool InputsAreValid(string name, Supervisor supervisor, out string errorMessage) {

            if (name.Length < 2) {
                errorMessage = "Name needs to be atleast two characters, please try again.";
                return false;
            }

            if(supervisor == null) {
                errorMessage = "No supervisor selected, please select one.";
                return false;
            }

            errorMessage = "";
            return true;
        }

        /// <summary>
        ///     Sets the Forms field value "studentToEdit".
        /// </summary>
        /// <param name="student">
        ///     The student to edit.
        ///     Send in an new Student() if you 
        ///     want to create a new Student.
        /// </param>
        public void SetStudentToEdit(Student student) {

            if(student == null) {
                student = new Student();
            }

            this.studentToEdit = student;

            this.nameTextBox.Text = student.Name;
            this.supervisorComboBox.SelectedItem = student.Supervisor; 
        }



        /// <summary>
        ///     Adds all supervisors in the list to the 
        ///     ComboBox in the CreateAndEditForm.
        /// </summary>
        /// <param name="supervisors">
        ///     List of supervisors for the user to choose from.
        /// </param>
        public void AddSupervisorsToDropdownList(Supervisor[] supervisors) {
            this.supervisorComboBox.Items.AddRange(supervisors);
        }

        #endregion
    }
}
