﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExperisNoroffTask19.View {
    public partial class ConfirmChoiceForm : Form {

        private bool isOkClicked = false;


        public ConfirmChoiceForm(string messageToUser) {
            InitializeComponent();

            alertMessageLabel.Text = messageToUser;
        }

        private void ConfirmButton_Click(object sender, EventArgs e) {
            isOkClicked = true;
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e) {
            this.Close();
        }

        public bool IsOkClicked() { return isOkClicked; }
    }
}
