﻿using System.Windows.Forms;

namespace ExperisNoroffTask19.View {
    public class AlertForm : Form {
        private Button confirmButton;
        private Label alertMessageLabel;
        private Label headlineLabel;


        public AlertForm(string alertMessage) {
            InitializeComponent();

            alertMessageLabel.Text = alertMessage;
        }

        private void InitializeComponent() {
            this.headlineLabel = new System.Windows.Forms.Label();
            this.confirmButton = new System.Windows.Forms.Button();
            this.alertMessageLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // headlineLabel
            // 
            this.headlineLabel.AutoSize = true;
            this.headlineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headlineLabel.Location = new System.Drawing.Point(24, 31);
            this.headlineLabel.Name = "headlineLabel";
            this.headlineLabel.Size = new System.Drawing.Size(192, 29);
            this.headlineLabel.TabIndex = 0;
            this.headlineLabel.Text = "An error occured";
            // 
            // confirmButton
            // 
            this.confirmButton.Location = new System.Drawing.Point(84, 162);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(179, 48);
            this.confirmButton.TabIndex = 2;
            this.confirmButton.Text = "OK";
            this.confirmButton.UseVisualStyleBackColor = true;
            this.confirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // alertMessageLabel
            // 
            this.alertMessageLabel.AutoSize = true;
            this.alertMessageLabel.Location = new System.Drawing.Point(29, 76);
            this.alertMessageLabel.MaximumSize = new System.Drawing.Size(290, 0);
            this.alertMessageLabel.MinimumSize = new System.Drawing.Size(290, 0);
            this.alertMessageLabel.Name = "alertMessageLabel";
            this.alertMessageLabel.Size = new System.Drawing.Size(290, 34);
            this.alertMessageLabel.TabIndex = 3;
            this.alertMessageLabel.Text = "asdfkasd klasdf kasdf mlasdf mklasdf mklasdf mklasd fmklasdf ømkl";
            // 
            // AlertForm
            // 
            this.ClientSize = new System.Drawing.Size(349, 222);
            this.Controls.Add(this.alertMessageLabel);
            this.Controls.Add(this.confirmButton);
            this.Controls.Add(this.headlineLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AlertForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void ConfirmButton_Click(object sender, System.EventArgs e) {
            this.Close();
        }
    }
}